package it.unibo.oop.lab.exception1;

public class NotEnoughBatteryException extends IllegalStateException {
	
	private static final long serialVersionUID = -6255135725778650366L;
    private final double batteryLevel;
    private final double batteryRequired;
    
	public NotEnoughBatteryException(double batteryLevel, double batteyRequired) {
		super();
		this.batteryLevel = batteryLevel;
		this.batteryRequired = batteyRequired;
	}
	
	public String getMessage() {
		return "No enough battery for moving. Battery level is " + batteryLevel + " battery required is " + batteryRequired;
    }
    

}
