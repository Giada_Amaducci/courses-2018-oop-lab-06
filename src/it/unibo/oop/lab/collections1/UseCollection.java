package it.unibo.oop.lab.collections1;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Example class using {@link List} and {@link Map}.
 * 
 */
public final class UseCollection {

	private static int INIZIO = 1000;
	private static int FINE = 2000;
	private static final int ELEMS = 100000;
    private static final int TO_MS = 100000;
    private UseCollection() {
    }

    /**
     * @param s
     *            unused
     */
    public static void main(final String... s) {
    	
    	 /*
         * 1) Create a new ArrayList<Integer>, and populate it with the numbers
         * from 1000 (included) to 2000 (excluded).
         */
    	List<Integer> array = new ArrayList<>();
    	
    	for (int i = INIZIO; i < FINE; i++) {
            array.add(i);
        }
    	
    	System.out.println(array);
    	
    	/*
         * 2) Create a new LinkedList<Integer> and, in a single line of code
         * without using any looping construct (for, while), populate it with
         * the same contents of the list of point 1.
         */
    	List<Integer> list = new LinkedList<>(array);
    	
    	System.out.println(list);
    	
    	/*
         * 3) Using "set" and "get" and "size" methods, swap the first and last
         * element of the first list. You can not use any "magic number".
         * (Suggestion: use a temporary variable)
         */
    	int lastElem = array.get(array.size() - 1);
    	array.set(array.size() -1, array.get(0));
    	array.set(0, lastElem);

    	/*
         * 4) Using a single for-each, print the contents of the arraylist.
         */
    	for (int elem: array) {
    		System.out.print(elem + " ");
    	}
        
        /*
         * 5) Measure the performance of inserting new elements in the head of
         * the collection: measure the time required to add 100.000 elements as
         * first element of the collection for both ArrayList and LinkedList,
         * using the previous lists. In order to measure times, use as example
         * TestPerformance.java.
         */
    	long time = System.nanoTime();
    	
    	for (int i = 0; i <= ELEMS; i++) {
            array.add(0, i);
        }
    	time = System.nanoTime() - time;
        System.out.println("\nAdd " + ELEMS
                + " in position 0 " + time
                + "ns (" + time / TO_MS + "ms)");
        
        time = System.nanoTime();
    	
    	for (int i = 0; i <= ELEMS; i++) {
            list.add(0, i);
        }
    	time = System.nanoTime() - time;
        System.out.println("Add " + ELEMS
                + " in position 0 " + time
                + "ns (" + time / TO_MS + "ms)");
    	
        /*
         * 6) Measure the performance of reading 1000 times an element whose
         * position is in the middle of the collection for both ArrayList and
         * LinkedList, using the collections of point 5. In order to measure
         * times, use as example TestPerformance.java.
         */
        
        time = System.nanoTime();
    	
    	for (int i = 0; i <= 1000; i++) {
            array.get(array.size()/2);
        }
    	time = System.nanoTime() - time;
        System.out.println("\nGet " + ELEMS
                + " in position 0 " + time
                + "ns (" + time / TO_MS + "ms)");
        
        time = System.nanoTime();
    	
    	for (int i = 0; i <= 1000; i++) {
            list.get(list.size()/2);
        }
    	time = System.nanoTime() - time;
        /*System.out.println("Get " + ELEMS
                + " in position 0 " + time
                + "ns (" + time / TO_MS + "ms)");*/
        
        /*
         * 7) Build a new Map that associates to each continent's name its
         * population:
         * 
         * Africa -> 1,110,635,000
         * 
         * Americas -> 972,005,000
         * 
         * Antarctica -> 0
         * 
         * Asia -> 4,298,723,000
         * 
         * Europe -> 742,452,000
         * 
         * Oceania -> 38,304,000
         */
       
        
         
         Map<String, Long> map = new HashMap<>();
         map.put("Africa", 1110635000L);
         map.put("Americas", 972005000L);
         map.put("Antarctica", 0L);
         map.put("Asia", 4298723000L);
         map.put("Europe", 742452000L);
         map.put("Oceania", 38304000L);
         
         long population = 0L;
         for (String elem : map.keySet()) {
        	 population += map.get(elem).longValue();
         }
         
         System.out.println(population);
         
         
        /*
         * 8) Compute the population of the world
         */
         
    }
}
